<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

   
    public function __construct()
    {
        parent::__construct();
        $this->load->model('MY_Model','mod');
        $this->load->model('MY_Datatable','mod_datatable');
        $this->get_login();
    }

    public function get_login()
    {
        
        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            if ($this->uri->segment(1)!='auth'){
                redirect('auth/login', 'refresh');
            }
            if ($this->uri->segment(2)=='logout') {
                redirect('auth/login', 'refresh');
            }
        }
        else 
        {            
            if ($this->uri->segment(2)!='logout') {
                $group_name=$this->ion_auth->get_users_groups()->row();
                if ($this->uri->segment(1)!=$group_name->name) {
                    redirect($group_name->name.'/Dashboard', 'refresh');
                }
            }
            
        }
    }

    public function data_all()
    {
        $data['profil_website']     =  $this->mod->get_profil_website(); 
        $data['user_account']       =  $this->ion_auth->user()->row_array();
        $data['user_groups']        =   $this->my_groups();
        $data['sidebar']            =   $this->my_sidebar();
        return $data;
    }

    public function my_template()
    {
        return $this->config->item('template');
    }

    public function my_login_page()
    {
        return $this->config->item('login_page');
    }

    public function my_groups()
    {
        return $this->ion_auth->get_users_groups()->row_array();
    }

    public function my_view($view,$data_get)
    {
        $i=0;
        $data = $this->data_all();
        $data['data_get']=$data_get;
        foreach ($view as $key => $value) {
            if ($i==0) {
                $this->load->view($value,$data);
            }else{
                $this->load->view($value);
            }
        }
    }
    public function my_delete_file($folder)
    {
            //Get a list of all of the file names in the folder.
            $files = glob($folder . '/*');
            //Loop through the file list.
            foreach($files as $file){
                //Make sure that this is a file and not a directory.
                if(is_file($file)){
                    //Use the unlink function to delete the file.
                    unlink($file);
                }
            }
    }
    public function my_sidebar()
    {
        $group  =   $this->my_groups();
        return 'role/'.$group['name'].'/include/'.$this->config->item('sidebar_name');;
    }
    public function my_update($tabel, $data, $where)
    {
        return $this->mod->set_update($tabel, $data, $where);
    }
    public function my_where($tabel, $where, $limit=0)
    {
        return $this->mod->get_where($tabel, $where, $limit);
    }
    public function my_db_count($tabel, $where)
    {
        return $this->mod->get_where($tabel, $where)->num_rows();
    }
    public function get_user_account()
    {
        return $this->ion_auth->user()->row_array();
    }

    public function save_data($tabel, $data)
    {
        return $this->mod->save($tabel, $data);
    }

    public function my_pdf($param)
    {
        /*
            param[
                'url',
                'customPaper',
                'data',
                'name',
                'pos' => 'landscape' / 'portrait'

            ];
        */
        
        $this->pdf->setPaper($param['customPaper'], $param['pos']);
        $this->pdf->load_view($param['url'], $param['data_value'], $param['name']);
        echo $param['name'];
        // print_r($param['data_value']);
    }

    public function my_export_excel($param)
    {
        $dt = $this->arr;
        $extension = pathinfo($param, PATHINFO_EXTENSION);
 
                if($extension == 'csv'){
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
                } elseif($extension == 'xlsx') {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                } else {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                }
                // file path
                $spreadsheet = $reader->load($_FILES['fileURL']['tmp_name']);
                $allDataInSheet = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
            
                // array Count
                $arrayCount = count($allDataInSheet);
                $flag = 0;
                $createArray = $dt['column'];
                $makeArray = [];
                foreach ($dt['column'] as $key => $value) {
                    $makeArray[$value] = $value;
                };

                $SheetDataKey = array();
                foreach ($allDataInSheet as $dataInSheet) {
                    foreach ($dataInSheet as $key => $value) {
                        if (in_array(trim($value), $createArray)) {
                            $value = preg_replace('/\s+/', '', $value);
                            $SheetDataKey[trim($value)] = $key;
                        } 
                    }
                }
                $dataDiff = array_diff_key($makeArray, $SheetDataKey);
                if (empty($dataDiff)) {
                    $flag = 1;
                }
                // match excel sheet column
                if ($flag == 1) {
                    for ($i = 2; $i <= $arrayCount; $i++) {
                        $addresses = array();
                        $dtcolumn=[];
                        foreach ($dt['column'] as $key => $value) {

                            $dtcolumn[$value] = $allDataInSheet[$i][$SheetDataKey[$value]];
                        }
                        $fetchData[] = $dtcolumn;
                    }   
                    $data['dataInfo'] = $fetchData;
                    return $fetchData;
                } else {
                    print_r($fetchData);
                    echo "Please import correct file, did not match excel sheet column";
                }
    }
    public function index()
    {
        $tmp    =  $this->my_template();
        $data   =  $this->data_all();

        $this->load->view('template/'.$tmp.'/head',$data);
        $this->load->view('template/'.$tmp.'/navbar');
        $this->load->view('template/'.$tmp.'/sidebar');
        $this->load->view('template/'.$tmp.'/content');
        $this->load->view('template/'.$tmp.'/footer');
    }

    public function call_datatable($data_get='')
    {
        $_POST['frm']   =   $data_get;
        $list           =   $this->mod_datatable->get_datatables();
        $data           =   array();
        $no             =   $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row        =   array();
            $row[]      =   '<input type="checkbox" name="get-check" value="'.$field[$data_get['id']].'"></input>';
            foreach ($data_get['column'] as $key => $value_row) {
                $row[]  =   $field[$value_row];
            }
            $data[]     =   $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mod_datatable->count_all(),
            "recordsFiltered" => $this->mod_datatable->count_filtered(),
            "data" => $data,
        );

        return $output;
    }

    public function display_view($page = "index_table", $additional_data = [])
    {
        $data['account']    =   $this->get_user_account();
        $data['param']      =   $this->arr;
        $data['additional_data'] = $additional_data;
        $this->my_view(['role/core_page/'.$page.'/index','role/core_page/'.$page.'/js'],$data);
    }

    public function save_data_param()
    {
        $param = $this->arr;

        $data_save = [];
        foreach ($param['column'] as $key => $value) {
            $data_save[$value] = $_POST[$value];
        };

        return $this->save_data($param['table'], $data_save);
    }

    public function save_data_batch($table='', $data="")
    {
        return $this->db->insert_batch($table, $data);
    }

    public function save_media($data)
    {
        $config['upload_path']=$data['path'] ; //path folder file upload
        $config['allowed_types']='gif|jpg|png|jpeg|PNG|JPG|JPEG|pdf|doc|docx|xls|xlsx'; //type file yang boleh di upload
        $config['encrypt_name'] = TRUE; //enkripsi file name upload
         
        $this->load->library('upload',$config); //call library upload 
        if($this->upload->do_upload($data['filename'])){ //upload file
           
            return $this->upload->data(); 
        }

    }
    public function save_my_jurnal($data)
    {
        if (isset($data)) {
            if (!empty($data)) {
               $ins = [];
                foreach ($data['referensi'] as $key => $value) {
                    $ins[] = [
                        'ref'           =>  $data['ref'],
                        'keterangan'    =>  $data['keterangan'],
                        'table'         =>  $data['table'],
                        'idtable_fk'    =>  $data['idtable_fk'],
                        'akun'          =>  $value['akun'],
                        'debit'         =>  $value['debit'],
                        'kredit'        =>  $value['kredit'],
                    ];
                }
                return $this->db->insert_batch('jurnal_umum', $ins);
            }
        }
        
    }
}