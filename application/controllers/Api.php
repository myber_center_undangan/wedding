<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends MY_Frontend {
  
  function curl($url, $key)
  {
    $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "key: $key"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        return "cURL Error #:" . $err;
      } else {
        return $response;
      }
  }

  function curl_post($url, $key, $post)
  {
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $post,
      CURLOPT_HTTPHEADER => array(
        "content-type: application/x-www-form-urlencoded",
        "key: $key"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      return "cURL Error #:" . $err;
    } else {
      return $response;
    }
  }

  function get_province()
  {
    $send = "<option value=''>--Pilih--</option>";
    $data = json_decode($this->curl("https://api.rajaongkir.com/starter/province", "fc20d4b7fd3cbf217890058b52e882e1"));
    foreach ($data->rajaongkir->results as $key => $value) {
        $send.="<option value='".$value->province_id."'>".$value->province."</option>";
    }
    echo $send;
    // print_r($data) ;
  }

  function get_city()
  {
    $send = "<option value=''>--Pilih--</option>";
    $data = json_decode($this->curl("https://api.rajaongkir.com/starter/city?province=".$_POST['id_province'], "fc20d4b7fd3cbf217890058b52e882e1"));
    foreach ($data->rajaongkir->results as $key => $value) {
        $send.="<option value='".$value->city_id."'>".(($value->type=="Kabupaten") ? "Kabupaten":"Kota").' '.$value->city_name."</option>";
    }
    echo $send;
  }

  function get_cost()
  {
    $kota=   $this->db->get_where('additional_setting',["key_add" => "city_id"])->row_array();

    $post = "origin=".$kota['value_add']."&destination=".$_POST['destination']."&weight=1700&courier=".$_POST['ekspedisi'];
    
    $data['pengiriman'] = json_decode($this->curl_post("https://api.rajaongkir.com/starter/cost", "fc20d4b7fd3cbf217890058b52e882e1", $post));
    $this->load->view('role/checkout/opsi_pengiriman',$data);

  }

  function add_pengiriman()
  {
    $pengiriman = 
    [
        "key_add"     => "jenis_pengiriman",
        "value_add"   => json_encode([
            [
              "text"  =>  "POS Indonesia",
              "val"   =>  "pos"
            ],
            [
              "text"  =>  "JNE",
              "val"   =>  "jne"
            ],
            [
              "text"  =>  "Tiki",
              "val"   =>  "tiki"
            ],
        ])
    ];
    $this->db->insert("additional_setting", $pengiriman);
  }

  function add_bank()
  {
    $pengiriman = 
    [
        "key_add"     => "jenis_pembayaran",
        "value_add"   => json_encode([
            [
              "text"      =>  "Bank BCA",
              "no_rek"    =>  "12321312312312",
              "atas_nama" =>  "Rendy Yani Susanto"
            ],
            [
              "text"      =>  "Bank BRI",
              "no_rek"    =>  "43432",
              "atas_nama" =>  "Cicik Winarsih"],
            [
              "text"      =>  "Bank BNI",
              "no_rek"    =>  "56566",
              "atas_nama" =>  "Rendy Yani Susanto"
            ],
        ])
    ];
    $this->db->insert("additional_setting", $pengiriman);
  }
  
}

