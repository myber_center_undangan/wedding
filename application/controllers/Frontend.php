<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends MY_Frontend {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('MY_Model','mod');
        $this->load->library('session');
    }

    function index($lang = "")
    {
        $setting_table = [];
        
        if ($lang == "") {
            $lang = $this->db->get('profil_website')->row_array()['default_language'];
        }
        
        foreach ($this->db->get_where('setting_table', ['table'=>'bisweel'])->result_array() as $key => $value) {
            $setting_table[$value['name']] = $value['value'.(($lang == "en") ? "_en" :"" )];
        }
        $data = [
            'profil_website'    =>  $this->db->get('profil_website')->row_array(),
            'blog_post'         =>  $this->db->limit(6)->get('blog_post')->result_array(),
            'link'              =>  $this->db->get('link')->result_array(),
            'galery'            =>  $this->db->get('galery')->result_array(),
            'setting_table'     =>  $setting_table,
            'blog_category'     =>  $this->db->get('blog_category')->result_array(),
            'slider'            =>  $this->db->get('slider')->result_array(),
            'keunggulan'        =>  $this->db->get('keunggulan')->result_array(),
            'kategori_produk'   =>  $this->db->get('kategori_produk')->result_array(),
            'produk'            =>  $this->db->limit(9)->get('produk')->result_array(),
            'testimoni'         =>  $this->db->get('testimoni')->result_array(),
            'page'              =>  'index'
        ];

        $this->load->view('template/bisweel/header', $data);
        $this->load->view('template/bisweel/nav');
        $this->load->view('role/frontend/bisweel/slider');
        // $this->load->view('role/frontend/bisweel/features');
        $this->load->view('role/frontend/bisweel/custom_page1');
        $this->load->view('role/frontend/bisweel/call_to_action');
        $this->load->view('role/frontend/bisweel/portofolio');
        // $this->load->view('role/frontend/bisweel/testi');
        // $this->load->view('role/frontend/bisweel/blog');
        $this->load->view('template/bisweel/footer');
    }
    function detail_blog($id, $lang = ""){
        $setting_table = [];
        if ($lang == "") {
            $lang = $this->db->get('profil_website')->row_array()['default_language'];
        }
        
        foreach ($this->db->get_where('setting_table', ['table'=>'bisweel'])->result_array() as $key => $value) {
            $setting_table[$value['name']] = $value['value'.(($lang == "en") ? "_en" :"" )];
        }
        $data = [
            'profil_website'        =>  $this->db->get('profil_website')->row_array(),
            'blog_post'        =>  $this->db->get_where('blog_post',['id_blog_post'=>$id])->row_array(),
            
            'link'              =>  $this->db->get('link')->result_array(),
            'setting_table' =>  $setting_table,

          
        ];

        $this->load->view('template/bisweel/header', $data);
        $this->load->view('template/bisweel/nav');
        $this->load->view('role/frontend/bisweel/blog_post/blog_detail');
        $this->load->view('template/bisweel/footer');
    }
    function detail_portofolio($id, $lang = ""){
        $setting_table = [];
        if ($lang == "") {
            $lang = $this->db->get('profil_website')->row_array()['default_language'];
        }
        
        foreach ($this->db->get_where('setting_table', ['table'=>'bisweel'])->result_array() as $key => $value) {
            $setting_table[$value['name']] = $value['value'.(($lang == "en") ? "_en" :"" )];
        }
        $data = [
            'profil_website'        =>  $this->db->get('profil_website')->row_array(),
            'produk'        =>  $this->db->get_where('produk',['id_produk'=>$id])->row_array(),
            
            'link'              =>  $this->db->get('link')->result_array(),
            'setting_table' =>  $setting_table,
          
        ];

        $this->load->view('template/bisweel/header', $data);
        $this->load->view('template/bisweel/nav');
        $this->load->view('role/frontend/bisweel/portofolio/portofolio_detail');
        $this->load->view('template/bisweel/footer');
    }

    function pages($link="", $lang = ""){
        $setting_table = [];
        if ($lang == "") {
            $lang = $this->db->get('profil_website')->row_array()['default_language'];
        }
        
        foreach ($this->db->get_where('setting_table', ['table'=>'bisweel'])->result_array() as $key => $value) {
            $setting_table[$value['name']] = $value['value'.(($lang == "en") ? "_en" :"" )];
        }
        $data = [
            'profil_website'        =>  $this->db->get('profil_website')->row_array(),
            'link'              =>  $this->db->get('link')->result_array(),
            'setting_table' =>  $setting_table,
            'pages'                 =>  $this->db->get_where('pages',['link'=>$link])->row_array()
        ];

        $this->load->view('template/bisweel/header', $data);
        $this->load->view('template/bisweel/nav');
        $this->load->view('role/frontend/bisweel/pages/all_pages');
        $this->load->view('template/bisweel/footer');
    }
    function about_us($lang = ""){
        $setting_table = [];
        foreach ($this->db->get_where('setting_table', ['table'=>'bisweel'])->result_array() as $key => $value) {
            $setting_table[$value['name']] = $value['value'];
        }
        $data = [
            'profil_website'        =>  $this->db->get('profil_website')->row_array(),
            'link'              =>  $this->db->get('link')->result_array(),
            'setting_table' =>  $setting_table,
            'keunggulan'        =>  $this->db->get('keunggulan')->result_array(),
        ];

        $this->load->view('template/bisweel/header', $data);
        $this->load->view('template/bisweel/nav');
        $this->load->view('role/frontend/bisweel/about_us/about_us');
        $this->load->view('template/bisweel/footer');
    }
    function products($lang = ""){
        $setting_table = [];
        if ($lang == "") {
            $lang = $this->db->get('profil_website')->row_array()['default_language'];
        }
        
        foreach ($this->db->get_where('setting_table', ['table'=>'bisweel'])->result_array() as $key => $value) {
            $setting_table[$value['name']] = $value['value'.(($lang == "en") ? "_en" :"" )];
        }
        $data = [
            'profil_website'        =>  $this->db->get('profil_website')->row_array(),
            'link'              =>  $this->db->get('link')->result_array(),
            'setting_table' =>  $setting_table,
            'kategori_produk'   =>  $this->db->get('kategori_produk')->result_array(),
            'produk'            =>  $this->db->get('produk')->result_array(),
        ];

        $this->load->view('template/bisweel/header', $data);
        $this->load->view('template/bisweel/nav');
        $this->load->view('role/frontend/bisweel/products/products');
        $this->load->view('template/bisweel/footer');
    }
    function contact_us($lang = ""){
        $setting_table = [];
        if ($lang == "") {
            $lang = $this->db->get('profil_website')->row_array()['default_language'];
        }
        
        foreach ($this->db->get_where('setting_table', ['table'=>'bisweel'])->result_array() as $key => $value) {
            $setting_table[$value['name']] = $value['value'.(($lang == "en") ? "_en" :"" )];
        }
        $data = [
            'profil_website'        =>  $this->db->get('profil_website')->row_array(),
            'link'              =>  $this->db->get('link')->result_array(),
            'setting_table' =>  $setting_table,
        ];

        $this->load->view('template/bisweel/header', $data);
        $this->load->view('template/bisweel/nav');
        $this->load->view('role/frontend/bisweel/contact_us/contact_us');
        $this->load->view('template/bisweel/footer');
    }
    function faq($lang = ""){
        $setting_table = [];
        if ($lang == "") {
            $lang = $this->db->get('profil_website')->row_array()['default_language'];
        }
        
        foreach ($this->db->get_where('setting_table', ['table'=>'bisweel'])->result_array() as $key => $value) {
            $setting_table[$value['name']] = $value['value'.(($lang == "en") ? "_en" :"" )];
        }
        $data = [
            'profil_website'        =>  $this->db->get('profil_website')->row_array(),
            'link'              =>  $this->db->get('link')->result_array(),
            'faq'              =>  $this->db->get('faq')->result_array(),
            'setting_table' =>  $setting_table,
        ];

        $this->load->view('template/bisweel/header', $data);
        $this->load->view('template/bisweel/nav');
        $this->load->view('role/frontend/bisweel/faq/faq');
        $this->load->view('template/bisweel/footer');
    }
    function galery($lang = ""){
        $setting_table = [];
        if ($lang == "") {
            $lang = $this->db->get('profil_website')->row_array()['default_language'];
        }
        
        foreach ($this->db->get_where('setting_table', ['table'=>'bisweel'])->result_array() as $key => $value) {
            $setting_table[$value['name']] = $value['value'.(($lang == "en") ? "_en" :"" )];
        }
        $data = [
            'profil_website'        =>  $this->db->get('profil_website')->row_array(),
            'link'              =>  $this->db->get('link')->result_array(),
            'galery'              =>  $this->db->get('galery')->result_array(),
            'galery_video'      =>  $this->db->get('galery_video')->result_array(),
            'setting_table' =>  $setting_table,
        ];

        $this->load->view('template/bisweel/header', $data);
        $this->load->view('template/bisweel/nav');
        $this->load->view('role/frontend/bisweel/galery/galery');
        $this->load->view('template/bisweel/footer');
    }
    public function galery_detail($id='', $lang = "")
    {
        $setting_table = [];
        if ($lang == "") {
            $lang = $this->db->get('profil_website')->row_array()['default_language'];
        }
        
        foreach ($this->db->get_where('setting_table', ['table'=>'bisweel'])->result_array() as $key => $value) {
            $setting_table[$value['name']] = $value['value'.(($lang == "en") ? "_en" :"" )];
        }
        $data = [
            'profil_website'        =>  $this->db->get('profil_website')->row_array(),
            'link'              =>  $this->db->get('link')->result_array(),
            'galery'              =>  $this->db->get_where('galery', ['id_galery'=>$id])->row_array(),
            'setting_table' =>  $setting_table,
        ];

        $this->load->view('template/bisweel/header', $data);
        $this->load->view('template/bisweel/nav');
        $this->load->view('role/frontend/bisweel/galery/galery_detail');
        $this->load->view('template/bisweel/footer');
    }
}
