<?php
defined('BASEPATH') OR exit('No direct script access allowed');
		use PhpOffice\PhpSpreadsheet\Spreadsheet;
		use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class kategori_produk extends MY_Controller {
	

	public $arr = [
			'title'				=>	'Halaman kategori_produk',
			'table'				=>	'kategori_produk',
			'column'			=>	[ 'kategori_produk'],
			'column_order'		=>	[ 'id_kategori_produk','kategori_produk'],
			'column_search'		=>	[ 'id_kategori_produk','kategori_produk'],
			'order'				=>	['id_kategori_produk'	=>	'DESC'],
			'id'				=>	'id_kategori_produk'
	];

	/*
		CHANGE PAGE
	*/
	public function get_data()
	{
		/*if you need custom page*/

		$data['account']	=	$this->get_user_account();
		$data['param'] 		= 	$this->arr;
		$this->my_view(['role/admin/page/kategori_produk/index_page/index','role/admin/page/kategori_produk/index_page/js'],$data);

	}

	public function add_page()
	{
		$data['account']	=	$this->get_user_account();
		$data['param'] 		= 	$this->arr;
		$this->my_view(['role/admin/page/kategori_produk/add_page/index','role/admin/page/kategori_produk/add_page/js'],$data);
	}

	public function edit_page($id)
	{
		$dt = $this->arr;

		$data['param'] 		= 	$this->arr;
		if (isset($id)) {
			$data_set = $this->my_where($dt['table'],[$dt['id']=>$id])->row_array();
			$data['data_edit']	=	$data_set;
			$this->my_view(['role/admin/page/kategori_produk/edit_page/index','role/admin/page/kategori_produk/edit_page/js'],$data);
		} else {
			$this->get_data();
		}
	}

	/*
		ADD DATA 
	*/


	public function simpan_data()
	{	
		if ($this->save_data_param()) {
			$this->get_data();
		}	else 	{
			echo "error";
		}
	}


	/*
		EDIT DATA
	*/

	function update_data()
	{
		if (isset($_POST)) {
			$this->my_update(
				'kategori_produk', 
				['kategori_produk'=>$_POST['kategori_produk']],
				['id_kategori_produk'=>$_POST['id_kategori_produk']]
			);
		}
	}

	/*
		DELETE DATA
	*/

	function hapus()
	{
		$dt = $this->arr;
		foreach ($_POST['data_get'] as $key => $value) {
			$this->db->delete($dt['table'],[$dt['id']=>$value]);
		}
	}


	public function datatable()
	{
		$_POST['frm']   =   $this->arr;
        $list           =   $this->mod_datatable->get_datatables();
        $data           =   array();
        $no             =   $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row        =   array();
            
            $row[]      =   '<input type="checkbox" name="get-check" value="'.$field['id_kategori_produk'].'"></input>';
            $row[]		=	$field['kategori_produk'];
            $data[]     =   $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mod_datatable->count_all(),
            "recordsFiltered" => $this->mod_datatable->count_filtered(),
            "data" => $data,
        );

        echo json_encode($output);
	}
	
	
}