<?php
defined('BASEPATH') OR exit('No direct script access allowed');
		use PhpOffice\PhpSpreadsheet\Spreadsheet;
		use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class blog_post extends MY_Controller {

	public $arr = [
			'title'				=>	'Halaman blog_post',
			'table'				=>	'blog_post',
			'column'			=>	[ 'title','tag','blog_post','img','create_at','idblogcategory_fk','visit_count'],
			'column_order'		=>	[ 'id_blog_post','title','tag','blog_post','img','create_at','idblogcategory_fk','visit_count'],
			'column_search'		=>	[ 'id_blog_post','title','tag','blog_post','img','create_at','idblogcategory_fk','visit_count'],
			'order'				=>	['id_blog_post'	=>	'DESC'],
			'id'				=>	'id_blog_post'
	];

	/*
		CHANGE PAGE
	*/
	public function get_data()
	{
		/*if you need custom page*/

		$data['account']	=	$this->get_user_account();
		$data['param'] 		= 	$this->arr;
		$this->my_view(['role/admin/page/blog_post/index_page/index','role/admin/page/blog_post/index_page/js'],$data);

	}

	public function add_page()
	{
		$data['account']	=	$this->get_user_account();
		$data['param'] 		= 	$this->arr;
		$data['blog_category']	=	$this->my_where('blog_category',[])->result_array();
		$this->my_view(['role/admin/page/blog_post/add_page/index','role/admin/page/blog_post/add_page/js'],$data);
	}

	public function edit_page($id)
	{
		$dt = $this->arr;

		$data['param'] 		= 	$this->arr;
		if (isset($id)) {
			$data_set = $this->my_where($dt['table'],[$dt['id']=>$id])->row_array();
			$data['data_edit']	=	$data_set;

			$data['blog_category']	=	$this->my_where('blog_category',[])->result_array();
			$this->my_view(['role/admin/page/blog_post/edit_page/index','role/admin/page/blog_post/edit_page/js'],$data);
		} else {
			$this->get_data();
		}
	}

	/*
		ADD DATA 
	*/


	public function simpan_data()
	{	
		if(file_exists($_FILES['foto']['tmp_name']) || is_uploaded_file($_FILES['foto']['tmp_name'])) {
			$foto = $this->save_media([
				'path'	=>	"./include/media/",
				'filename' => 'foto',
			]);
		}

		$data = [
			'title'					=>	$_POST['title'],
			'tag'					=>	$_POST['tag'],	
			'blog_post'				=>	$_POST['blog_post'],		
			'img'					=>	((isset($foto)) ? $foto['file_name'] : ''),
			'idblogcategory_fk'		=>	$_POST['idblogcategory_fk'],
			'visit_count'			=>	0
		];

		$this->save_data('blog_post', $data);
	}


	/*
		EDIT DATA
	*/

	function update_data()
	{
		if (isset($_POST)) {
			if(file_exists($_FILES['foto']['tmp_name']) || is_uploaded_file($_FILES['foto']['tmp_name'])) {
					$foto = $this->save_media([
						'path'	=>	"./include/media/",
						'filename' => 'foto',
					]);
				}

			$data = [
				'title'					=>	$_POST['title'],
				'tag'					=>	$_POST['tag'],	
				'blog_post'				=>	$_POST['blog_post'],		
				'idblogcategory_fk'		=>	$_POST['idblogcategory_fk'],
			];

			if(file_exists($_FILES['foto']['tmp_name']) || is_uploaded_file($_FILES['foto']['tmp_name'])) {
				$data['img'] = ((isset($foto)) ? $foto['file_name'] : '');
			}
			$this->my_update(
				'blog_post', 
				$data,
				['id_blog_post'=>$_POST['id_blog_post']]
			);
		}
	}

	/*
		DELETE DATA
	*/

	function hapus()
	{
		$dt = $this->arr;
		foreach ($_POST['data_get'] as $key => $value) {
			$this->db->delete($dt['table'],[$dt['id']=>$value]);
		}
	}


	public function datatable()
	{
		$_POST['frm']   =   $this->arr;
        $list           =   $this->mod_datatable->get_datatables();
        $data           =   array();
        $no             =   $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row        =   array();
            
            $row[]      =   '<input type="checkbox" name="get-check" value="'.$field['id_blog_post'].'"></input>';
            $row[]		=	'<img src="'.(base_url('include/media/'.$field['img'])).'" style="width:40px;height:40px;"> <a class="app-item" href="blog_post/edit_page/'.$field['id_blog_post'].'">'. $field['title'].'</a>';
            $row[]		=	$field['visit_count'];
            $data[]     =   $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mod_datatable->count_all(),
            "recordsFiltered" => $this->mod_datatable->count_filtered(),
            "data" => $data,
        );

        echo json_encode($output);
	}
	
	
}