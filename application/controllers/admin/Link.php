<?php
defined('BASEPATH') OR exit('No direct script access allowed');
		use PhpOffice\PhpSpreadsheet\Spreadsheet;
		use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class link extends MY_Controller {
	

	public $arr = [
			'title'				=>	'Halaman link',
			'table'				=>	'link',
			'column'			=>	[ 'link'],
			'column_order'		=>	[ 'id_link','link'],
			'column_search'		=>	[ 'id_link','link'],
			'order'				=>	['id_link'	=>	'DESC'],
			'id'				=>	'id_link'
	];

	/*
		CHANGE PAGE
	*/
	public function get_data()
	{
		/*if you need custom page*/

		$data['account']	=	$this->get_user_account();
		$data['param'] 		= 	$this->arr;
		$this->my_view(['role/admin/page/link/index_page/index','role/admin/page/link/index_page/js'],$data);

	}

	public function add_page()
	{
		$data['account']	=	$this->get_user_account();
		$data['param'] 		= 	$this->arr;
		$this->my_view(['role/admin/page/link/add_page/index','role/admin/page/link/add_page/js'],$data);
	}

	public function edit_page($id)
	{
		$dt = $this->arr;

		$data['param'] 		= 	$this->arr;
		if (isset($id)) {
			$data_set = $this->my_where($dt['table'],[$dt['id']=>$id])->row_array();
			$data['data_edit']	=	$data_set;
			$this->my_view(['role/admin/page/link/edit_page/index','role/admin/page/link/edit_page/js'],$data);
		} else {
			$this->get_data();
		}
	}

	/*
		ADD DATA 
	*/


	public function simpan_data()
	{	
		if ($this->save_data_param()) {
			$this->get_data();
		}	else 	{
			echo "error";
		}
	}


	/*
		EDIT DATA
	*/

	function update_data()
	{
		if (isset($_POST)) {
			$this->my_update(
				'link', 
				['link'=>$_POST['link']],
				['id_link'=>$_POST['id_link']]
			);
		}
	}

	/*
		DELETE DATA
	*/

	function hapus()
	{
		$dt = $this->arr;
		foreach ($_POST['data_get'] as $key => $value) {
			$this->db->delete($dt['table'],[$dt['id']=>$value]);
		}
	}


	public function datatable()
	{
		$_POST['frm']   =   $this->arr;
        $list           =   $this->mod_datatable->get_datatables();
        $data           =   array();
        $no             =   $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row        =   array();
            
            $row[]      =   '<input type="checkbox" name="get-check" value="'.$field['id_link'].'"></input>';
            $row[]		=	$field['link'];
            $data[]     =   $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mod_datatable->count_all(),
            "recordsFiltered" => $this->mod_datatable->count_filtered(),
            "data" => $data,
        );

        echo json_encode($output);
	}
	
	
}