
<body class="overflow_hidden">
  
<!-- Loader Start -->
<div class="loading-page">
	<div class="counter">
		<p><img src="<?php echo base_url('include/media/'.$template_setting[array_search('logo_head', $template_setting)]['value'])?>" alt="image"></p>
		<h2>0%</h2>
		<hr/>
	</div>
</div>
<!-- Loader End  -->

	
<!--  Header Start  -->
<header id="header-top_1">
	<div id="header-top">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-4 col-xs-12 header_top_text">
					<p><?= $profil_website['tagline'] ?></p>
				</div>
				<div class="col-md-8 col-sm-8 col-xs-12 text-right">
					<ul class="top_socials">
						<li><a href="<?= $profil_website['facebook'] ?>"><i class="fa fa-facebook"></i></a>
						</li>
						<li><a href="<?= $profil_website['youtube'] ?>"><i class="fa fa-youtube"></i></a>
						</li>
						<li><a href="<?= $profil_website['instagram'] ?>"><i class="fa fa-instagram"></i></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div id="header-bottom">
		<div class="container">
			<div class="row">
				<div class="col-md-2 hidden-xs hidden-sm">
					<a href="javascript:void(0)"><img src="<?php echo base_url('include/media/'.$template_setting[array_search('logo_head', $template_setting)]['value'])?>" alt="logo" />
					</a>
				</div>
				<div class="col-md-10 col-sm-12 col-xs-12">
					<div class="get-tuch text-left">
						<i class="fa fa-phone" aria-hidden="true"></i>
						<ul>
							<li>
								<h4>Phone Number</h4>
							</li>
							<li>
								<p><?= $profil_website['no_hp'] ?></p>
							</li>
						</ul>
					</div>
					<div class="get-tech-line">
						<img src="<?php echo base_url('include/template/edublog/images/')?>get-tuch-line.png" alt="line" />
					</div>
					<div class="get-tuch text-left">
						<i class="fa fa-envelope-o" aria-hidden="true"></i>
						<ul>
							<li>
								<h4>Alamat,</h4>
							</li>
							<li>
								<p><?= $profil_website['alamat'] ?></p>
							</li>
						</ul>
					</div>
					<div class="get-tech-line"><img src="<?php echo base_url('include/template/edublog/images/')?>get-tuch-line.png" alt="line" />
					</div>
					<div class="get-tuch text-left">
						<i class="fa fa-map-marker" aria-hidden="true"></i>
						<ul>
							<li>
								<h4>Email Address</h4>
							</li>
							<li>
								<p><a href="javascript:void(0)"><?= $profil_website['email'] ?></a>
								</p>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>