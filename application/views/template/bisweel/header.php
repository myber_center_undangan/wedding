<!DOCTYPE html>
<html lang="zxx">
	<head>
		<!-- Meta Tag -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name='copyright' content='pavilan'>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<!-- Title Tag  -->
		<title><?php echo $profil_website['nama_website'] ?></title>
		
		<!-- Favicon -->
		<link rel="icon" type="image/x-icon" href="<?php echo base_url('include/media/'.$profil_website['icon']);?>">
		
		<!-- Web Font -->
		<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
		
		<!-- Bizwheel Plugins CSS -->
		<link rel="stylesheet" href="<?php echo base_url('include/template/bisweel/css/');?>animate.min.css">
		<link rel="stylesheet" href="<?php echo base_url('include/template/bisweel/css/');?>bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url('include/template/bisweel/css/');?>cubeportfolio.min.css">
		<link rel="stylesheet" href="<?php echo base_url('include/template/bisweel/css/');?>font-awesome.css">
		<link rel="stylesheet" href="<?php echo base_url('include/template/bisweel/css/');?>jquery.fancybox.min.css">
		<link rel="stylesheet" href="<?php echo base_url('include/template/bisweel/css/');?>magnific-popup.min.css">
		<link rel="stylesheet" href="<?php echo base_url('include/template/bisweel/css/');?>owl-carousel.min.css">
		<link rel="stylesheet" href="<?php echo base_url('include/template/bisweel/css/');?>slicknav.min.css">

		<!-- Bizwheel Stylesheet -->  
		<link rel="stylesheet" href="<?php echo base_url('include/template/bisweel/css/');?>reset.css">
		<link rel="stylesheet" href="<?php echo base_url('include/template/bisweel/');?>style.css">
		<link rel="stylesheet" href="<?php echo base_url('include/template/bisweel/css/');?>responsive.css">

	</head>
	<body id="bg">
	
		<!-- Boxed Layout -->
		<div id="page" class="site boxed-layout"> 
		
		<!-- Preloader -->
		<!-- <div class="preeloader">
			<div class="preloader-spinner"></div>
		</div> -->
		<!--/ End Preloader -->
	
		<!-- Header -->
		<header class="header" >
			<!-- Topbar -->
			<div class="topbar" style="background:<?php echo $setting_table['topbar_color'] ?> !important;">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 col-12">
							<!-- Top Contact -->
							<div class="top-contact">
								<div class="single-contact" style="color: <?php echo $setting_table['topbar_text_color'] ?>;"><i class="fa fa-phone"></i>Phone: <?php echo $profil_website['no_hp'] ?></div> 
								<div class="single-contact" style="color: <?php echo $setting_table['topbar_text_color'] ?>;"><i class="fa fa-envelope-open"></i>Email: <?php echo $profil_website['email'] ?></div>	
								<div class="single-contact" style="color: <?php echo $setting_table['topbar_text_color'] ?>;"><i class="fa fa-clock-o"></i>Opening:(<?php echo $profil_website['jam_buka'] ?>)</div> 
							</div>
							<!-- End Top Contact -->
						</div>	
						<div class="col-lg-4 col-12">
							<div class="topbar-right">
								<!-- Social Icons -->
								<ul class="social-icons">
									<li  ><a href="<?= $profil_website['facebook']; ?>" target="__blank"><i style="color: <?php echo $setting_table['topbar_text_color'] ?> !important;" class="fa fa-facebook"></i></a></li>
									<li  ><a href="<?= $profil_website['twitter']; ?>" target="__blank"><i style="color: <?php echo $setting_table['topbar_text_color'] ?> !important;" class="fa fa-twitter"></i></a></li>
									<li  ><a href="<?= $profil_website['youtube']; ?>" target="__blank"><i style="color: <?php echo $setting_table['topbar_text_color'] ?> !important;" class="fa fa-youtube"></i></a></li>
									<li  ><a href="<?= $profil_website['instagram']; ?>" target="__blank"><i style="color: <?php echo $setting_table['topbar_text_color'] ?> !important;" class="fa fa-instagram"></i></a></li>
								</ul>															
								<div class="button">
									<a href="<?php echo base_url('frontend/contact_us') ?>" class="bizwheel-btn">Contact Us</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>