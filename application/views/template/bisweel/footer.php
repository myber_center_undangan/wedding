
		<footer class="footer" style="background-image:url('<?php echo base_url('include/template/bisweel/img/');?>map.png')">
			<!-- Footer Top -->
			<div class="footer-top">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-12">
							<!-- Footer About -->		
							<div class="single-widget footer-about widget">	
								<div class="logo">
									<div class="img-logo">
										<a class="logo" href="index.html">
											<img class="img-responsive" style="max-width: 60px;" src="<?php echo base_url('include/media/'.$profil_website['logo']);?>" alt="logo">
										</a>
									</div>
								</div>
								<div class="footer-widget-about-description">
									<p><?php echo $profil_website['tagline'];?></p>
								</div>	
								<div class="social">
									<!-- Social Icons -->
									<ul class="social-icons">
										<li><a class="facebook" href="<?php echo $profil_website['facebook'];?><" target="_blank"><i class="fa fa-facebook"></i></a></li>
										<li><a class="twitter" href="<?php echo $profil_website['twitter'];?><" target="_blank"><i class="fa fa-twitter"></i></a></li>
										<li><a class="youtube" href="<?php echo $profil_website['youtube'];?><" target="_blank"><i class="fa fa-youtube"></i></a></li>
										<li><a class="instagram" href="<?php echo $profil_website['instagram'];?><" target="_blank"><i class="fa fa-instagram"></i></a></li>
									</ul>
								</div>
								<div class="button"><a href="#" class="bizwheel-btn">About Us</a></div>
							</div>		
							<!--/ End Footer About -->		
						</div>
						<div class="col-lg-6 col-md-6 col-12">	
							<!-- Footer Contact -->		
							<div class="single-widget footer_contact widget">	
								<h3 class="widget-title">Contact</h3>
								<p><?php echo $setting_table['footer_contact_text'];?></p>
								<ul class="address-widget-list">
									<li class="footer-mobile-number"><i class="fa fa-phone"></i><?php echo $profil_website['no_hp'];?></li>
									<li class="footer-mobile-number"><i class="fa fa-envelope"></i><?php echo $profil_website['email'];?></li>
									<li class="footer-mobile-number"><i class="fa fa-map-marker"></i><?php echo $profil_website['alamat'];?></li>
								</ul>
							</div>		
							<!--/ End Footer Contact -->						
						</div>
					</div>
					
				</div>
			</div>
			<!-- Copyright -->
			<div class="copyright">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<div class="copyright-content">
								<!-- Copyright Text -->
								<p>© Copyright <a href="myber.web.id">myber</a>. Design &amp; Development By <a target="_blank" href="#">ThemeLamp</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--/ End Copyright -->
		</footer>
		<a target="__blank" href="https://wa.me/<?php echo $profil_website['no_hp'] ?>?text=<?php echo rawurlencode($profil_website['wa_text']) ?>"class="floatapp" >
			<i class="fa fa-whatsapp my-float"></i> 
		</a>
		<style type="text/css">
			.floatapp {
			    position: fixed;
			    width: 60px;
			    height: 60px;
			    float: left;
			    left: 15px;
			    bottom: 15px;
			    background-color: #25d366;
			    color: #FFF;
			    border-radius: 50px;
			    text-align: center;
			    font-size: 30px;
			    box-shadow: 2px 2px 3px #999;
			    z-index: 100;
			}
			.my-float{
				margin-top: 16px;
			}
		</style>
		<!-- Jquery JS -->
		<script src="<?php echo base_url('include/template/bisweel/js/');?>jquery.min.js"></script>
		<script src="<?php echo base_url('include/template/bisweel/js/');?>jquery-migrate-3.0.0.js"></script>
		<!-- Popper JS -->
		<script src="<?php echo base_url('include/template/bisweel/js/');?>popper.min.js"></script>
		<!-- Bootstrap JS -->
		<script src="<?php echo base_url('include/template/bisweel/js/');?>bootstrap.min.js"></script>
		<!-- Modernizr JS -->
		<script src="<?php echo base_url('include/template/bisweel/js/');?>modernizr.min.js"></script>
		<!-- ScrollUp JS -->
		<!-- FacnyBox JS -->
		<script src="<?php echo base_url('include/template/bisweel/js/');?>jquery-fancybox.min.js"></script>
		<!-- Cube Portfolio JS -->
		<script src="<?php echo base_url('include/template/bisweel/js/');?>cubeportfolio.min.js"></script>
		<!-- Slick Nav JS -->
		<script src="<?php echo base_url('include/template/bisweel/js/');?>slicknav.min.js"></script>
		<!-- Slick Nav JS -->
		<script src="<?php echo base_url('include/template/bisweel/js/');?>slicknav.min.js"></script>
		<!-- Slick Slider JS -->
		<script src="<?php echo base_url('include/template/bisweel/js/');?>owl-carousel.min.js"></script>
		<!-- Easing JS -->
		<script src="<?php echo base_url('include/template/bisweel/js/');?>easing.js"></script>
		<!-- Magnipic Popup JS -->
		<script src="<?php echo base_url('include/template/bisweel/js/');?>magnific-popup.min.js"></script>
		<!-- Active JS -->
		<script src="<?php echo base_url('include/template/bisweel/js/');?>active.js"></script>
		<!--Start of Tawk.to Script-->
		<script type="text/javascript">
		var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
		(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/61e0c1d3b84f7301d32aeb38/1fpava8qf';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
		})();
		</script>
		<!--End of Tawk.to Script-->
	</body>
</html>