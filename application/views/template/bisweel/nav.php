
			<!-- Middle Header -->
			<div class="middle-header">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<div class="middle-inner">
								<div class="row">
									<div class="col-lg-2 col-md-3 col-12">
										<!-- Logo -->
										<div class="logo">
											<!-- Image Logo -->
											<div class="img-logo" style="max-width: 50px;">
												<a href="<?php echo base_url('frontend') ?>">
													<img src="<?php echo base_url('include/media/'.$profil_website['logo']);?>" alt="#">
												</a>
											</div>
										</div>								
										<div class="mobile-nav"></div>
									</div>
									<div class="col-lg-10 col-md-9 col-12">
										<div class="menu-area">
											<!-- Main Menu -->
											<nav class="navbar navbar-expand-lg">
												<div class="navbar-collapse">	
													<div class="nav-inner">	
														<div class="menu-home-menu-container">
															<!-- Naviagiton -->
															<ul id="nav" class="nav main-menu menu navbar-nav">
																
																<?php foreach ($link as $key => $value): ?>
																	<li><a href="<?php echo base_url($value['path']) ?>"><?php echo $value['name'] ?></a></li>
																<?php endforeach ?>
																<!-- <li class="icon-active"><a href="#">Blog</a>
																	<ul class="sub-menu">
																		<li><a href="blog.html">Blog Grid</a></li>
																		<li><a href="blog-single.html">Blog Single</a></li>
																	</ul>
																</li>
																<li class="icon-active"><a href="#">Pages</a>
																	<ul class="sub-menu">
																		<li><a href="about.html">About Us</a></li>
																		<li><a href="404.html">404</a></li>
																	</ul>
																</li> -->
															</ul>
															<!--/ End Naviagiton -->
														</div>
													</div>
												</div>
											</nav>
											<!--/ End Main Menu -->	
											<!-- Right Bar -->
											<div class="right-bar">
												<!-- Search Bar -->
												<ul class="right-nav">
													<li class="top-search"><a href="<?php echo base_url('frontend/index/id') ?>"><img src="<?php echo base_url('include/media/indonesia.png') ?>"></a></li>
													<li class="top-search"><a  href="<?php echo base_url('frontend/index/en') ?>"><img src="<?php echo base_url('include/media/united-kingdom.png') ?>"></a></li>
												</ul>
												
											</div>	
											<!--/ End Right Bar -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--/ End Middle Header -->
			
		</header>
		<!--/ End Header -->