
    <!-- jquery plugins here-->
    <!-- jquery -->
    <script src="<?php echo base_url('include/template/sasu/js/')?>jquery-1.12.1.min.js"></script>
    <!-- popper js -->
    <script src="<?php echo base_url('include/template/sasu/js/')?>popper.min.js"></script>
    <!-- bootstrap js -->
    <script src="<?php echo base_url('include/template/sasu/js/')?>bootstrap.min.js"></script>
    <!-- easing js -->
    <script src="<?php echo base_url('include/template/sasu/js/')?>jquery.magnific-popup.js"></script>
    <!-- swiper js -->
    <script src="<?php echo base_url('include/template/sasu/js/')?>swiper.min.js"></script>
    <!-- swiper js -->
    <script src="<?php echo base_url('include/template/sasu/js/')?>masonry.pkgd.js"></script>
    <!-- particles js -->
    <script src="<?php echo base_url('include/template/sasu/js/')?>owl.carousel.min.js"></script>
    <script src="<?php echo base_url('include/template/sasu/js/')?>jquery.nice-select.min.js"></script>
    <!-- slick js -->
    <script src="<?php echo base_url('include/template/sasu/js/')?>slick.min.js"></script>
    <script src="<?php echo base_url('include/template/sasu/js/')?>jquery.counterup.min.js"></script>
    <script src="<?php echo base_url('include/template/sasu/js/')?>waypoints.min.js"></script>
    <script src="<?php echo base_url('include/template/sasu/js/')?>contact.js"></script>
    <script src="<?php echo base_url('include/template/sasu/js/')?>jquery.ajaxchimp.min.js"></script>
    <script src="<?php echo base_url('include/template/sasu/js/')?>jquery.form.js"></script>
    <script src="<?php echo base_url('include/template/sasu/js/')?>jquery.validate.min.js"></script>
    <script src="<?php echo base_url('include/template/sasu/js/')?>mail-script.js"></script>
    <!-- custom js -->
    <script src="<?php echo base_url('include/template/sasu/js/')?>custom.js"></script>
</body>

</html>