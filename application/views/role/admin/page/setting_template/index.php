<script type="text/javascript" src="<?php echo base_url('include/ckeditor/ckeditor.js'); ?>"></script>
<form class="form-horizontal" action="<?php echo $data_get['param']['table'] ?>/update_data" id="app-submit" method="POST">

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-body">
      <fieldset>
              <?php foreach ($data_get['setting_template'] as $key => $value): ?>
                
              
              <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo $value['name'] ?></label>
                <div class="col-lg-4">
                  <input type="text" name="s<?php echo $value['id_setting_table'] ?>" value="<?php echo $value['value'] ?>" class="form-control" required placeholder="Input here......">
                </div>
                <div class="col-lg-4">
                  <input type="text" name="s_en<?php echo $value['id_setting_table'] ?>" value="<?php echo $value['value_en'] ?>" class="form-control" required placeholder="Input here......">
                </div>
              </div>
              <?php endforeach ?>
              <button class="btn btn-success" type="submit">Simpan</button>
            </fieldset> 
    </div>
  </div>
</div>
</form>