<form class="form-horizontal" action="<?php echo $data_get['param']['table'] ?>/simpan_data" id="app-submit" method="POST">

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-body">
			<fieldset>
	           <div class="form-group">
	              <label class="col-lg-3 control-label">Judul (Title):</label>
	              <div class="col-lg-6">
	                <input type="text" name="title"  required class="form-control" required placeholder="Input here......">
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Slider:</label>
	              <div class="col-lg-6">
	                <input type="file" name="img"  required class="form-control" required placeholder="Input here......">
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Deskripsi (Description):</label>
	              <div class="col-lg-9">
                	<textarea name="deskripsi" class="form-control" required id="deskripsi"></textarea>
	              </div>
	            </div>
	            <button class="btn btn-success" type="submit">Simpan</button>
            </fieldset>	
		</div>
	</div>
</div>
</form>