<script type="text/javascript" src="<?php echo base_url('include/ckeditor/ckeditor.js'); ?>"></script>
<form class="form-horizontal" action="<?php echo $data_get['param']['table'] ?>/update_data" id="app-submit" method="POST">

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-body">
			<fieldset>
				<input type="hidden" value="<?php echo $data_get['data_edit']['id_blog_post'] ?>" name="id_blog_post">
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Judul (Title):</label>
	              <div class="col-lg-6">
	                <input type="text" value="<?php echo $data_get['data_edit']['title'] ?>"  name="title" class="form-control" required placeholder="Input here......">
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Deskripsi (Description):</label>
	              <div class="col-lg-9">
                	<textarea name="blog_post"  required id="blog_post"><?php echo $data_get['data_edit']['blog_post'] ?></textarea>
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Tag (Tag):</label>
	              <div class="col-lg-6">
	                <input type="text" value="<?php echo $data_get['data_edit']['tag'] ?>"  name="tag"  required class="form-control" required placeholder="Input here......">
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Gambar Sampul (Cover):</label>
	              <div class="col-lg-6">
	                <input type="file" name="foto" class="form-control"  placeholder="Input here......">
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Kategori (Category):</label>
	              <div class="col-lg-6">
	              	<select class="form-control" required  name="idblogcategory_fk">
	              		<?php foreach ($data_get['blog_category'] as $value): ?>
	              			<option <?php echo ($data_get['data_edit']['idblogcategory_fk'] == $value['id_blog_category']) ? "selected" : "" ; ?> value="<?= $value['id_blog_category'] ?>"><?= $value['blog_category'] ?></option>
	              		<?php endforeach ?>
	              	</select>
	              </div>
	            </div>
	            <button class="btn btn-success" type="submit">Simpan</button>
            </fieldset>	
		</div>
	</div>
</div>
</form>