<ul class="navigation navigation-main navigation-accordion">
  <!-- Main -->
  <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
  <li><a href="Dashboard/get_data" class="app-item"><i class="icon-home4"></i> <span>Dashboard</span></a></li>

  <li>
    <a href="#"><i class="icon-magazine"></i> <span>POST</span></a>
    <ul>
      <li><a href="Blog_post/get_data" class="app-item">All Post</a></li>
      <li><a href="Blog_category/get_data" class="app-item">Category</a></li>
    </ul>
  </li>

  <li>
    <a href="#"><i class="icon-cube"></i> <span>Product</span></a>
    <ul>
      <li><a href="Produk/get_data" class="app-item">All Product</a></li>
      <li><a href="kategori_produk/get_data" class="app-item">Category</a></li>
    </ul>
  </li>

  <li>
    <a href="#"><i class="icon-stack-empty"></i> <span>Modules</span></a>
    <ul>
      <li><a href="Slider/get_data" class="app-item">Slider</a></li>
      <li><a href="Keunggulan/get_data" class="app-item">Keunggulan</a></li>
      <li><a href="Testimoni/get_data" class="app-item">Testimoni</a></li>
      <li><a href="Galery/get_data" class="app-item">Galery</a></li>
      <li><a href="Pages/get_data" class="app-item">Pages</a></li>
      <li><a href="Faq/get_data" class="app-item">Faq</a></li>


    </ul>
  </li>


  <!-- Page kits -->
  <li class="navigation-header"><span>Setting kits</span> <i class="icon-menu" title="Page kits"></i></li>
  
  <li>
    <a href="#"><i class="icon-office"></i> <span>General</span></a>
    <ul>
      <li><a href="Setting_template/get_data" class="app-item">Setting Template</a></li>
      <li><a href="Profil_website/get_data" class="app-item">Profil Website</a></li>
    </ul>
  </li>
  <li>
    <a href="#"><i class="icon-grid6"></i> <span>Account</span></a>
    <ul>
      <li><a href="users/get_data" class="app-item">User</a></li>
    </ul>
  </li>
  <!-- <li><a href="Media/get_data" class="app-item"><i class="icon-grid6"></i> <span>Media</span></a></li> -->
  <!-- /page kits -->
</ul>