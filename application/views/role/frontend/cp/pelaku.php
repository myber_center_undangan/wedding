

    <!-- breadcrumb start-->
    <section class="breadcrumb breadcrumb_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner text-center">
                        <div class="breadcrumb_iner_item">
                            <h2>Pelaku Dalam Badan MIB</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- breadcrumb start-->

    <!-- about_us part start-->
    <section class="about_us section_padding">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>pe1.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>pe2.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>pe3.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>pe4.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>pe5.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>pe6.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>pe7.png" alt="">
                    </div>
                </div>
            </div>
            
        </div>
       
    </section>
    <!-- about_us part end-->
