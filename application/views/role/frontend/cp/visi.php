
    <!-- breadcrumb start-->
    <section class="breadcrumb breadcrumb_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner text-center">
                        <div class="breadcrumb_iner_item">
                            <h2>Visi dan Misi</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- breadcrumb start-->

    <!-- about_us part start-->
    <section class="about_us section_padding">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-6 col-lg-5">
                    <div class="about_us_text">
                        <h2>Visi Indonesia Bangkit</h2>
                        <p>Menjadi solusi bagi tantangan Indonesia di masa depan dengan memanfaatkan potensi sektor unggul di Indonesia seperti Forestry, Agriculture, Aqua-Culture, Textile, Tourism, Energi dan Ekonomi Digital melalui pengembangan konsep Indonesia Bangkit</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>PETAINDONESIA.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <img src="<?php echo base_url('include/template/sasu/img/')?>left_sharp.png" alt="" class="left_shape_1">
        <img src="<?php echo base_url('include/template/sasu/img/')?>about_shape.png" alt="" class="about_shape_1">
        <img src="<?php echo base_url('include/template/sasu/img/')?>animate_icon/Shape-16.png" alt="" class="feature_icon_1">
        <img src="<?php echo base_url('include/template/sasu/img/')?>animate_icon/Shape-1.png" alt="" class="feature_icon_4">
    </section>
    <!-- about_us part end-->

   <section class="about_us section_padding">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-6 col-lg-6">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>art.png" alt="">
                    </div>
                </div>
                <div class="col-md-6 col-lg-5">
                    <div class="about_us_text">
                        <h2>Misi Indonesia Bangkit</h2>
                        <ol>
                            <li>Membangkitkan Indonesia melalui pengembangan generasi milenial</li>
                            <li>Mempersiapkan setiap milenial untuk menjadi pasukan perang Ekonomi Indonesia</li>
                            <li>Menolak faham yang tidak sejalan dengan target pengembangan setiap milenial yang ada maupun yang akan dilahirkan</li>
                            <li>Menjadikan Indonesia sebagai lumbung Papan, Pangan, Sandang, dan Energi dunia</li>
                        </ol>
                    </div>
                </div>
                
            </div>
        </div>
        <img src="<?php echo base_url('include/template/sasu/img/')?>left_sharp.png" alt="" class="left_shape_1">
        <img src="<?php echo base_url('include/template/sasu/img/')?>about_shape.png" alt="" class="about_shape_1">
        <img src="<?php echo base_url('include/template/sasu/img/')?>animate_icon/Shape-16.png" alt="" class="feature_icon_1">
        <img src="<?php echo base_url('include/template/sasu/img/')?>animate_icon/Shape-1.png" alt="" class="feature_icon_4">
    </section>