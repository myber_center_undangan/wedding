<section class="news-area archive blog-single section-padding">
			<div class="container">
				<div class="row">
					<div class="col-lg-10 offset-lg-1 col-md-10 offset-md-1 col-12">
						<div class="row">
							<div class="col-12">
								<div class="blog-single-main">
									<div class="main-image">
										<center><img src="<?php echo base_url('include/media/'.$produk['foto']) ?>" alt=""></center>
									</div>
									<div class="blog-detail">
										<!-- News meta -->
										
										<h2><?php echo $produk['nama'] ?></h2>
										<?php echo $produk['deskripsi'] ?>

										<div class="posts_nav">
											<div class="post-left"><a target="__blank" href="https://wa.me/<?php echo $profil_website['no_hp'] ?>?text=<?php echo rawurlencode($profil_website['wa_text_product'].' '.$produk['nama']) ?>"><i class="fa fa-whatsapp"></i> Get Information About this Products</a></div>
										</div>
									</div>
								</div>
							</div>
						</div>
										
					</div>		
				</div>
			</div>
		</section>	