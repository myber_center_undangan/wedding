<section class="news-area archive blog-single section-padding">
			<div class="container">
				<div class="row">
					<div class="col-lg-10 offset-lg-1 col-md-10 offset-md-1 col-12">
						<div class="row">
							<div class="col-12">
								<div class="blog-single-main">
									<div class="main-image">
										<img src="<?php echo base_url('include/media/'.$blog_post['img']) ?>" alt="<?php echo $blog_post['tag'] ?>">
									</div>
									<div class="blog-detail">
										<!-- News meta -->
										<ul class="news-meta">
											<li><i class="fa fa-user"></i>Admin</li>
											<li><i class="fa fa-pencil"></i><?php echo date_format(date_create($blog_post['create_at']), 'd M Y') ?></li>
											<li><i class="fa fa-comments"></i>0 comments</li>
										</ul>
										<h2><?php echo $blog_post['title'] ?></h2>
										<?php echo $blog_post['blog_post'] ?>
									</div>
								</div>
							</div>
						</div>
						<!-- <div class="row">
							<div class="col-12">
								<div class="blog-comments-form">
									<div class="bottom-title">
										<h2>Leave a comment</h2>
										<p>All fields marked with an asterisk (*) are required </p>
									</div>
									<form class="form" method="post" action="mail/mail.php">
										<div class="row">
											<div class="col-lg-4 col-md-4 col-12">
												<div class="form-group">
													<label>Your Name<span>*</span></label>
													<input type="text" name="name" required="required">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-12">
												<div class="form-group">
													<label>Your Email<span>*</span></label>
													<input type="email" name="email" required="required">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-12">
												<div class="form-group">
													<label>Your Website<span>*</span></label>
													<input type="url" name="website" required="required">
												</div>
											</div>
											<div class="col-12">
												<div class="form-group">
													<label>Your Comment<span>*</span></label>
													<textarea name="message" rows="6"></textarea>
												</div>
											</div>
											<div class="col-12">
												<div class="form-group button">	
													<button type="submit" class="bizwheel-btn primary effect">Submit Comment<i class="fa fa-paper-plane"></i></button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>							
						</div>	 -->						
					</div>		
				</div>
			</div>
		</section>	