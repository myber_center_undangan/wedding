	<!-- About Us -->
		<section class="about-us section-space">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 offset-lg-1 col-md-6 col-12">
						<!-- About Video -->
							
								<img src="<?= base_url('include/media/b1.jpg') ?>" style="border-radius: 5%;" class="img img-responsive">
								
						<!--/End About Video  -->
					</div>
					<div class="col-lg-5 col-md-6 col-12">
						<div class="about-content section-title default text-left">
							<div class="section-top">
								<h1><b>HariBahagiaku.com</b></h1>
							</div>
							<div class="section-bottom">
								<div class="text">
									<p>Kami siap menjadi partner anda dalam memeriahkan pesta pernikahan. Kami menyediakan jasa pembuatan undangan digital dalam bentuk video, poster, banner dan website</p>
								</div>
								<div class="button">
									<a href="about.html" class="bizwheel-btn theme-2">Lihat Produk Kami<i class="fa fa-angle-right"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>	
		<!--/ End About Us -->