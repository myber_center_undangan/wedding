
		
		<!-- Hero Slider -->
		<section class="hero-slider style1">
			<div class="home-slider">
				<!-- Single Slider -->
				
				<!--/ End Single Slider -->
				<!-- Single Slider -->
				<?php foreach ($slider as $key => $value): ?>
					
					<div class="single-slider" style="background:url(<?php echo base_url('include/media/'.$value['img']); ?>);background-size: cover;">
						<div class="container">
							<div class="row">
								<div class="col-lg-7 col-md-8 col-12">
									<div class="welcome-text"> 
										<div class="hero-text"> 
											<h1 style="color:#fff !important;"><?php echo $value['title'] ?></h1>
											<div class="p-text" >
												<p style="color:#fff !important;"><?php echo $value['deskripsi'] ?></p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach ?>
				
			</div>
		</section>
		<!--/ End Hero Slider -->
		