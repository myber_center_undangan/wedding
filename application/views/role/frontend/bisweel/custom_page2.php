	<!-- About Us -->
		<section class="about-us section-space">
			<div class="container">
				<div class="row">
					
					<div class="col-lg-5 col-md-6 col-12">
						<div class="about-content section-title default text-left">
							<div class="section-top">
								<h1><span>Owner Coconut Charcoal World</span><b>We Provide Quality Business &amp; Smart Solution</b></h1>
							</div>
							<div class="section-bottom">
								<div class="text">
									<p>"Jangan takut untuk bermimpi. Karena mimpi adalah kesuksesan awal yang harus dilakukan. Banyak orang sudah gagal diawal karena untuk bermimpi saja dia tidak berani"</p>
								</div>
								<div class="button">
									<a href="#" class="bizwheel-btn theme-2">Owner CCW<i class="fa fa-angle-right"></i></a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-5 offset-lg-1 col-md-6 col-12">
						<!-- About Video -->
						<div class="modern-img-feature">
							<img src="<?php echo base_url('include/media/owner2.jpg') ?>" alt="#">
							
						</div>
						<!--/End About Video  -->
					</div>
				</div>
			</div>
		</section>	
		<!--/ End About Us -->