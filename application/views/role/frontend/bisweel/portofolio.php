
		<!-- Portfolio -->
		<section class="portfolio section-space">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-12">
						<div class="section-title default text-center">
							<div class="section-top">
								<h1><b><?php echo $setting_table['portofolio_section_text_title'] ?></b></h1>
							</div>
							<div class="section-bottom">
								<div class="text">
									<p><?php echo $setting_table['portofolio_section_text_body'] ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="portfolio-menu">
							<!-- Portfolio Nav -->
							<ul id="portfolio-nav" class="portfolio-nav tr-list list-inline cbp-l-filters-work">
								<li data-filter="*" class="cbp-filter-item active">All</li>
								<?php foreach ($kategori_produk as $key => $value): ?>
									<li data-filter=".k<?php echo $value['id_kategori_produk'] ?>" class="cbp-filter-item"><?php echo $value['kategori_produk'] ?></li>
								<?php endforeach ?>
							</ul>
							<!--/ End Portfolio Nav -->
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="portfolio-main">
							<div id="portfolio-item" class="portfolio-item-active">
								<?php foreach ($produk as $key => $value): ?>
									
								
								<div class="cbp-item k<?php echo $value['idkategoriproduk_fk'] ?> animation">
									<!-- Single Portfolio -->
									<div class="single-portfolio">
										<div class="portfolio-head overlay">
											<img src="<?php echo base_url('include/media/'.$value['foto']) ?>" alt="#">
											<a class="more" href="<?php echo base_url('frontend/detail_portofolio/'.$value['id_produk']); ?>"><i class="fa fa-long-arrow-right"></i></a>
											<a class="morewa" target="__blank" href="https://wa.me/<?php echo $profil_website['no_hp'] ?>?text=<?php echo rawurlencode($profil_website['wa_text_product'].' '.$value['nama']) ?>"><i class="fa fa-whatsapp"></i></a>

										</div>
										<div class="portfolio-content" style="border-left-color: <?php echo $setting_table['portofolio_section_body_product_color'] ?> !important ;">
											<h4><a href="<?php echo base_url('frontend/detail_portofolio/'.$value['id_produk']); ?>"><?php echo $value['nama'] ?></a></h4>

											<b style="color: #c0392b !important;">Rp. <?= number_format($value['harga'], 0, '.','.') ?></b>
											
										</div>
									</div>
									<!--/ End Single Portfolio -->
								</div>

								<?php endforeach ?>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/ End Portfolio -->