

	<!--  Latest News Start  -->
	<section id="latest_news" class="padding_bottom padding_top">
		<div class="container">

			<div class="row">
				<div class="col-md-12 text-center">
					<div class="heading margin_bottom">
						<h2><?= $post_title ?></h2>
						<p><?= $post_text ?></p>
					</div>
				</div>
			</div>

			<div class="row">
				<div id="news_slider">
					<?php foreach ($blog_post as $value): ?>
						<div class="item services_box">

							<div class="services_img">
								<img src="<?php echo base_url('include/media/'.$value['img'])?>" alt="Owl Image">
							</div>
							<div class="news_image ">
								<span><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <?= $value['tag'] ?></span>
								<span class="text-right"><i class="fa fa-calendar" aria-hidden="true"></i> <?= date_format(date_create($value['create_at']), 'm-Y') ?></span>

							</div>
							<div class="news_detail" style="padding-top:15px !important;">
								<h3><a href="javascript:void(0)"><?= substr($value['title'],0,100);  ?></a></h3>
								<?= substr($value['blog_post'],0,200); ?>
							</div>

						</div>
					<?php endforeach ?>
					


				</div>
			</div>

		</div>
	</section>
	<!--  Latest News End  -->
