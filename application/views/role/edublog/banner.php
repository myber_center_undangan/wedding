
<!--  Banner Start  -->
<section class="rev_slider_wrapper">
	<div id="rev_slider_3" class="rev_slider" data-version="5.0">
		<ul>
			<!-- SLIDE  -->
			<?php foreach ($banner as $value): ?>
				<li data-transition="fade">
					<img src="<?php echo base_url('include/media/'.$value['value'])?>" alt="" data-bgposition="center center" data-bgfit="cover" class="rev-slidebg">
					
				</li>
			<?php endforeach ?>
		</ul>
	</div>

</section>
<!--  Banner End  -->


<!--  Purchase Now Start  -->
<section class="purchase_now">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-8 col-xs-12">
				<div class="purchase_text">
					<h3><?= $tag_head ?></h3>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12 text-right">
				<div class="purchase_button">
					<a href="javascript:void(0)" class="btn_dark">Daftar Sekarang</a>
				</div>
			</div>
		</div>
	</div>
</section>
<!--  Purchase Now End  -->
